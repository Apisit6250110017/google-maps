import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late GoogleMapController mapController;

  final LatLng _center = const LatLng(7.518834274634801, 99.57882689922963);

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }
  Set<Marker> _createMarker() {
    return {
      Marker(
          markerId: MarkerId("marker_1"),
        position: LatLng(7.415055096429034, 100.15366346358536),
          infoWindow: InfoWindow(title: 'ที่อยู่/บ้านของนักศึกษา',
          snippet: '30 ,ม.6 ต.โคกสัก อ.บางแก้ว จ.พัทลัง'),
          ),
      Marker(
          markerId: MarkerId("marker_2"),
          position: LatLng(7.518857802118131, 99.57888735717877),
          infoWindow: InfoWindow(title: 'มหาวิทยาลัยนักศึกษา',
    snippet: 'มหาวิทยาลัยสงขลานครินทร์ วิทยาเขตตรัง'),

      ),
    };
  }
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Google Maps '),
          backgroundColor: Colors.green[700],
        ),
        body: GoogleMap(
      myLocationEnabled: true,
          mapToolbarEnabled: true,
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: _center,
            zoom: 15.0,
          ),
        markers: _createMarker(),
        ),
      ),
    );
  }
}